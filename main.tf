# Create resource group
resource "azurerm_resource_group" "rg" {
  name     = "${var.resource_prefix}-RG"
  location = var.node_location
}

#Create a virtual network
resource "azurerm_virtual_network" "vnet" {
  name                = "${var.resource_prefix}-vnet"
  address_space       = var.node_address_space
  location            = var.node_location
  resource_group_name = azurerm_resource_group.rg.name
}

#Create subent
resource "azurerm_subnet" "subnet" {
  name                 = "${var.resource_prefix}-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.node_address_prefix
}

#publicIP address 
resource "azurerm_public_ip" "publicIP" {
  count               = var.node_count
  name                = "${var.resource_prefix}-${format("%02d", count.index)}-PublicIP"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = var.Environment == "Test" ? "Static" : "Dynamic"
  tags = {
    "environment" = "test"
  }
}

resource "azurerm_network_security_group" "newNSG" {
  name                = "${var.resource_prefix}-NSG"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  security_rule {
    access                     = "Allow"
    description                = "SSH inbound rule"
    destination_address_prefix = "*"
    destination_port_range     = "22"
    direction                  = "Inbound"
    name                       = "SSH"
    priority                   = 100
    protocol                   = "Tcp"
    source_address_prefix      = "*"
    source_port_range          = "*"
  }

  security_rule {
    access                     = "Allow"
    description                = "RDP inbound rule"
    destination_address_prefix = "*"
    destination_port_range     = "3389"
    direction                  = "Inbound"
    name                       = "RDP"
    priority                   = 200
    protocol                   = "Tcp"
    source_address_prefix      = "*"
    source_port_range          = "*"
  }
  security_rule {
    access                     = "Allow"
    description                = "WinRM inbound rule"
    destination_address_prefix = "*"
    destination_port_range     = "5986"
    direction                  = "Inbound"
    name                       = "WinRM"
    priority                   = 300
    protocol                   = "Tcp"
    source_address_prefix      = "*"
    source_port_range          = "*"
  }

  tags = {
    "environment" = "test"
  }
}

resource "azurerm_subnet_network_security_group_association" "subnet_nsg_association" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.newNSG.id

}

#vm-nic0 for linux vm0
resource "azurerm_network_interface" "nic" {
  count               = var.node_count
  name                = "${var.resource_prefix}-${format("%02d", count.index)}-NIC"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = element(azurerm_public_ip.publicIP.*.id, count.index)
  }
}

data "template_file" "auto_logon" {
  template = file("./source/tpl.auto_logon.xml")

  vars = {
    admin_username = var.admin_username
    admin_password = var.admin_password
  }
}

#Create a new Linux VM
resource "azurerm_virtual_machine" "vm" {
  count                         = var.node_count
  name                          = "${var.resource_prefix}-${format("%02d", count.index)}"
  location                      = azurerm_resource_group.rg.location
  resource_group_name           = azurerm_resource_group.rg.name
  network_interface_ids         = [element(azurerm_network_interface.nic.*.id, count.index)]
  vm_size                       = "Standard_D2s_v3"
  delete_os_disk_on_termination = true

  storage_os_disk {
    name              = "OsDisk-${count.index}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_lRS"
  }

  storage_image_reference {
    # publisher = "Canonical"
    # offer     = "UbuntuServer"
    # sku       = "18.04-LTS"
    # version   = "latest"

    # publisher = "MicrosoftWindowsServer"
    # offer     = "WindowsServer"
    # sku       = "2019-Datacenter"
    # version   = "latest"

    publisher = "RedHat"
    offer     = "RHEL"
    sku       = "7-LVM"
    version   = "latest"
  }

  # os_profile {
  #   computer_name  = "node-${format("%02d", count.index)}"
  #   admin_username = var.admin_username
  #   admin_password = var.admin_password
  #   custom_data    = file("./source/ConfigureRemotingForAnsible.ps1")
  # }

  os_profile {
    computer_name  = "node-${format("%02d", count.index)}"
    admin_username = var.admin_username
    admin_password = var.admin_password
    # custom_data    = file("./source/ConfigureRemotingForAnsible.ps1")
  }

  os_profile_linux_config {
    disable_password_authentication = false
    ssh_keys {
      key_data = file("~/.ssh/id_rsa.pub")
      path     = "/home/${var.admin_username}/.ssh/authorized_keys"
    }
  }
  # os_profile_windows_config {
  #   provision_vm_agent = true
  #   additional_unattend_config {
  #     pass         = "oobeSystem"
  #     component    = "Microsoft-Windows-Shell-Setup"
  #     setting_name = "AutoLogon"
  #     content      = data.template_file.auto_logon.rendered
  #   }

  #   additional_unattend_config {
  #     pass         = "oobeSystem"
  #     component    = "Microsoft-Windows-Shell-Setup"
  #     setting_name = "FirstLogonCommands"
  #     content      = file("./source/tpl.first_logon_commands.xml")
  #   }
  # }
}

resource "azurerm_managed_disk" "disk1" {
  name                 = "${azurerm_virtual_machine.vm[0].name}-disk1"
  location             = azurerm_resource_group.rg.location
  resource_group_name  = azurerm_resource_group.rg.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 400
}

resource "azurerm_virtual_machine_data_disk_attachment" "example" {
  managed_disk_id    = azurerm_managed_disk.disk1.id
  virtual_machine_id = azurerm_virtual_machine.vm[0].id
  lun                = "10"
  caching            = "ReadWrite"
}

output "public_ip_address" {
  value      = azurerm_public_ip.publicIP.*.ip_address
  depends_on = [azurerm_public_ip.publicIP]
}
