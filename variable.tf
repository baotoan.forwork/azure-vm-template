variable "resource_group_name" {
  type = string
}

variable "node_location" {
  type = string
}

variable "resource_prefix" {
  type = string
}

variable "node_address_space" {
  default = ["1.0.0.0/16"]
}

# Variable for network range
variable "node_address_prefix" {
  default = ["1.0.1.0/24"]
}

# Variable for Environment
variable "Environment" {
  type = string
}

variable "node_count" {
  type = number
}

variable "client_secret" {
  type = string
}

variable "admin_username" {
  type        = string
  description = "administrator user name for virtual machine"
  default     = "lebaotoan"
}

variable "admin_password" {
  type        = string
  description = "Password must meet Azure complexity requirements"
  default     = "Password1234!"
}

