provider "azurerm" {
  # subscription_id = var.subscription_id
  # client_id       = var.client_id
  # tenant_id       = var.tenant_id
  subscription_id = "497915ff-abd8-457e-8249-1d5d33a7b725"
  client_id       = "38dc2723-98bd-47ed-8ce7-65dd4ba247a4"
  tenant_id       = "baa355bd-85ef-413b-963b-4c93d16da4cc"
  client_secret   = var.client_secret
  features {}
}

terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      # version = "~> 2.26"
      version = ">2.9.0"
    }
  }
}
#   backend "azurerm" {
#     resource_group_name  = "terraformstate"
#     storage_account_name = "azlbttfstate"
#     container_name       = "remotestate"
#     key                  = "k8snode.tfstate"
#   }
# }


